package com.example.viewpagerwork3.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpagerwork3.fragments.Fragment1
import com.example.viewpagerwork3.fragments.Fragment2
import com.example.viewpagerwork3.fragments.Fragment3

class ViewPagerFragmentAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0->{
                Fragment1()
            }
            1->{
                Fragment2()
            }
            2->{
                Fragment3()
            }
            else->{
                Fragment()
            }
        }
    }
}